# Changelog

## v1.1.0
### Changed
  * README.md instruction updates
  * Fix issue where audio durations were appearing invalid in Firefox
  * Fix issue where dynamic token changes were not being updated in the request.

# v1.0.0
### Added
  *  Initial Release