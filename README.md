# <ev-personalised-video\>

Video player element for the EValue's Personalised Videos. This widget utilizes Façade API within EValue's API Portal.

## Browser Support
---

Our personalised video player has been tested with the following browsers:

* **Google Chorme**
* **Firefox**
* **Microsoft Edge**
* **Safari**
* **Internet Explorer 11**

The player also supports iPads and iPhones using the Safari browser. For other tablets, the Chrome browser is supported. Please note, we do not support the tablets' native browsers.

## Connecting to EValue's APIs
---
First you need to sign up to the Portal, which you can do on our website, using just your name and email.

https://api-store.evalueproduction.com/store/site/pages/sign-up.jag

## Subscribing to an API
---
Sign into the EValue Developer Portal and then follow the detailed instructions at [EValue Portal – Getting Started – Subscribing to individual APIs](https://api.ev.uk/getting-started.php#individualApis). Please contact support@ev.uk, to grant you access to the Façade API.

## Generating Access Keys
---
Once you’ve signed up, you need to generate the access key required for authentication. This can be done by following the instructions on the [EValue Developer Portal](https://api.ev.uk/getting-started.php), under the section [‘Generating access keys’](https://api.ev.uk/getting-started.php#generatingKeys).


## Installation
---

Although the ev-personalised-video is a bower package, it is not registered in the bower registry. To install it you will need to tell bower CLI where to look for the package. So intead of running

```sh
$ bower install –-save ev-personalised-video-public
```
you will need to run:

```sh
$ bower install --save https://bitbucket.org/evuk/ev-personalised-video-public.git
```
Alternatively, you can create a bower.json by hand

```json
{
    "name": "your-application",
    "dependencies": {
		"ev-personalised-video-public": "https://bitbucket.org/evuk/ev-personalised-video-public.git#v1.0.0"
    }
}
```
Then run ```bower install``` in the directory where the bower.json resides.

## Bundled Version
---

The ES5 transpiled bundled version of the video player can be found in the ```bower_components/ev-personalised-video-public/dist```

## Including Video Player in HTML Example
---

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes">
    <title>Personalised video demo</title>
    <link rel="import" href="bower_components/ev-personalised-video-public/ev-personalised-video.html">
    <script src="bower_components/webcomponentsjs/webcomponents-loader.js" defer></script>
  </head>
  <body>
    <div>
      <h3>Personalised video demo</h3>
      <ev-personalised-video></ev-personalised-video>
    </div>
  </body>
</html>
```
## Shortcuts
---

The video player accomodates keyboard shortcuts to handle common playback options:
- `Space` Key (play/pause the video)
- `F` Key (Enter/Exit fullscreen mode)
- `M` Key (Mute/Unmute audio)


## Proxy Service Endpoint
---

If you wish to conceal the authentication token from client side inspection, you can provide your own proxy service endpoint
to incercept, create and forward the API Facade request. You **cannot** define both the authentication token and proxy service endpoint.

```html
<ev-personalised-video proxy="http://www.myproxyendpoint.co.uk/resource"></ev-personalised-video>
```

## Styling
---

The following custom CSS variables are also available for custom styling:

Custom property | Description | Default
------------------------------------------|-------------------------------------------------------------|----------------------
`--ev-player-focus-color`                 | Color for the highlight when track controls are focussed    | `#83b3fd`
`--ev-player-min-width`                   | Minimum width of the video player                           | `340px`
`--ev-player-track-controls-min-width`    | Minimum width of the track controls                         | `300px`
`--ev-player-svg-container-min-height`    | Minimum height of the svg animation container               | `720px`
`--ev-track-bar-container-height`         | Height of the track bar container                           | `20px`
`--ev-track-controls-background-color`    | Background color for the video controls                     | `#fafafa`
`--ev-track-icons-color`                  | Color for the svg track control icons                       | `black`
`--ev-track-fill-color`                   | Color for the track bar                                     | `#5a5a5a`
`--ev-track-pointer-color`                | Color for the circular pointer on the track bar             | `--ev-cornflower-500`
`--ev-track-progress-color`               | Color for the progress made on the track bar                | `--ev-cornflower-500`
`--ev-large-play-button-background-color` | Background color of the large play button                   | `--ev-cornflower-500`
`--ev-large-play-button-hover-color`      | Background color of the large play button on hover          | `--ev-cornflower-700`
`--ev-large-play-button-width`            | Width of the large play button                              | `120px`
`--ev-large-play-button-height`           | Height radius of the large play button                      | `80px`
`--ev-large-play-button-border-radius`    | Border radius of the large play button                      | `3px`
`--ev-video-large-play-button`            | CSS Mixin for styling the large play button                 | ``
`--ev-unsupported-msg-background-color`   | Background color the unsupported device message             | `--ev-icon-fill`
`--ev-unsupported-msg-icon-color`         | Icon color of the unsupported device message                | `--ev-cornflower-500`
`--ev-unsupported-msg-heading-color`      | Text color of the heading of the unsupported device message | `--ev-cornflower-500`
`--ev-unsupported-msg-heading-color`      | Text color of the heading of the unsupported device message | `--ev-cornflower-500`
`--ev-spinner-outer-color`                | Outer color of the loading spinner                          | `#FFFFFF`
`--ev-spinner-inner-color`                | Inner color of the loading spinner                          | `#9b46d6`
`--ev-spinner-logo-display`               | Display of the logo within the loading spinner              | `block`
`--ev-initialize-button-width`            | Width of the initialization button                          | `74px`
`--ev-initialize-button-height`           | Height of the initialization button                         | `148px`
`--ev-initialize-button-border-radius`    | Border radius of the initialization button                  | `3px`
`--ev-initialize-button-icon`             | CSS Mixin for styling the initialization icon               | ``

Here is a example of how to define some custom CSS variables

```html
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes">
    <title>Personalised video demo</title>
    <script src="bower_components/webcomponentsjs/webcomponents-loader.js"></script>
    <script src="bower_components/ev-personalised-video-public/ev-personalised-video.html"></script>
    <!-- custom-style element invokes the custom properties polyfill -->
    <script src="bower_components/polymer/lib/elements/custom-style.html"></script>
  </head>

 <!-- ensure that custom props are polyfilled on browsers that don't support them -->
  <custom-style>
    <style>
      ev-personalised-video {
        --ev-spinner-inner-color: #00dfb3;
        --ev-spinner-logo-display: none;
        --ev-personalised-video-large-play-button: {
          height: 100px;
          width: 100px;
          border-radius: 50%;
          background: #0c7aef;
        };
      }
    </style>
    ...
  </custom-style>
```

## Initialization Text
----

There is the option to slot in your own initialization text which will override the default text.
Example below:

```html
<ev-personalised-video id="video1" authentication-token="">
  <div class="initization-message" slot="initialization-text">
    <p>Load Video</p>
    <span>This may take some time</span>
  </div>
</ev-personalised-video>
```

## Icons
---

Currently the video player uses default SVG Icons from EValue's Media Iconset, however you can override them to use
your own iconset or you can provide a url to a PNG file.


```html
<link rel="import" href="bower_components/iron-icons/iron-icons.html">
<link rel="import" href="bower_components/ev-personalised-video-public/ev-personalised-video.html">
    ...
    <ev-personalised-video id="video"></ev-personalised-video>
    ...
    <script>
    const videoElement = document.getElementById('video');
    videoElement.config = {
          icons: {
            largePlayIcon: 'search',
            pauseIcon : 'https://cdn.onlinewebfonts.com/svg/img_377100.png'
          }
    }
    .....
    </script>
```

Below are all the names of the icons you can override

Icon Name        | Description            | Default
-----------------|------------------------|----------------------------------------
`largePlayIcon`  | Large play button icon | `ev-icons-extra-large:large-play-arrow`
`initializeIcon` | Initialization button icon | `ev-icons-large:initialize`
`playIcon`       | Play button icon       | `ev-media:play-arrow`
`pauseIcon`      | Pause button icon      | `ev-media:pause`
`replayIcon`     | Replay button icon     | `ev-media:replay`
`volumeIcon`     | Volume button icon     | `ev-media:volume-up`
`mutedIcon`      | Muted button icon      | `ev-media:volume-off`
`fullscreenIcon` | Fullscreen button icon | `ev-media:fullscreen`
`fullscreenOffIcon` | Fullscreen off button icon | `ev-media:fullscreen-exit`
`subtitleIcon`   | Subtitle button icon   | `ev-media:subtitles`
`subtitleOffIcon`   | Subtitle off button icon   | `ev-media:subtitles-off`

## Custom Events
---
There are 2 custom events of the video player which can be listened for in the local DOM:

* `player-loaded` - when all video resources have loaded successfully and are ready to be played
* `player-error` - when there is API error response when initializing the:
*  video player

Example:

```javascript
  const player.addEventListener('player-loaded', (event) => {
    // do something...
  });
```

## Polyfills
---

As HTML Web Components use a set of new standards, these are not natively supported by older browsers and require a set of polyfills so that all features work as expected. The polyfills Javascript library contains a ‘loader’ script which will check the user’s browser to see which of the four main specifications that web components are based on are supported. It will then load the polyfills required for the component to display and function correctly.

Here are a list of polyfills you might want to include:

**webcomponents-lite.js** includes all of the polyfills necessary to run on any of the supported browsers. Because all browsers receive all polyfills, there is an extra overhead when using this.

**webcomponents-loader.js** performs client-side feature-detection and loads just the required polyfills. This requires an extra round-trip to the server but saves bandwidth for browsers that support one or more features.

**custom-elements-es5-adapter.js** essentially wraps ES5 compiled Custom Elements to work across browsers. This polyfill must load **before** defining a ES5 Custom Element. This adapter will automatically wrap ES5. This adapter must **NOT** be compiled.

```html
<script src="bower_components/webcomponentsjs/custom-elements-es5-adapter.js"></script>
<script src="bower_components/webcomponentsjs/webcomponents-loader.js"></script>
```

Click [here](https://github.com/webcomponents/webcomponentsjs) to learn more about **webcomponentsloader.js** and the other polyfills listed above.


## Caveats
---
For iOS & Safari to emit sound from HTML5 Audio, it's critical to have a user-triggered event (button click) once the initialization process
is complete. This occurs when the user initially clicks on the large play button. By setting the ```auto-play``` attribute to the video, the player bypasses
this step, possibly resulting in inaudible videos.